<html>
    <head>
        <title>UniaBR Database</title>

        <link href='//fonts.googleapis.com/css?family=Lato:100' rel='stylesheet' type='text/css'>

        <style>
            body {
                margin: 0;
                padding: 0;
                width: 100%;
                height: 100%;
                color: #B0BEC5;
                display: table;
                font-weight: 500;

            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 96px;
                margin-bottom: 40px;
                font-family: 'Lato';
            }

            .item {
                font-size: 24px;
                font-weight: 400;
                margin: 10px;
            }

            .item strong {
                font-weight: 900;
            }

            .credit {
                font-weight: 700;
                position: absolute;
                bottom: 30px;
                left: 45%;
                font-family: 'Lato';
            }

            .credit a {
                text-decoration: none;
                color: grey;
            }

            .credit strong {
                font-weight: 900;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content text-center">
                <div class="title">UniaBR Database</div>
            </div>
        </div>
    </body>
</html>
